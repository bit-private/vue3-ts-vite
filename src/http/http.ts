import request from './axios'

// get
const GET = function (url: string, params?: object) {
  return new Promise((resolve, reject) => {
    request
      .get(url, {
        params: params
      })
      .then(res => {
        resolve(res)
      })
      .catch(err => {
        reject(err)
      })
  })
}
// post--json
const postJson = function (url: string, data: object) {
  return new Promise((resolve, reject) => {
    request({
      url,
      method: 'post',
      headers: {
        'Content-Type': 'application/json'
      },
      data
    })
      .then(res => {
        resolve(res)
      })
      .catch(err => {
        reject(err)
      })
  })
}
// post--Fromdata
const postFrom = function (url: string, data: object) {
  return new Promise((resolve, reject) => {
    request({
      url,
      method: 'post',
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8'
      },
      transformRequest: [
        function (data) {
          let ret = ''
          for (let it in data) {
            ret += encodeURIComponent(it) + '=' + encodeURIComponent(data[it]) + '&'
          }
          return ret
        }
      ],
      data
    })
      .then(res => {
        resolve(res)
      })
      .catch(err => {
        reject(err)
      })
  })
}
const DELETE = function (url: string, data: object) {
  return new Promise((resolve, reject) => {
    request({
      url,
      method: 'delete',
      data
    })
      .then(res => {
        resolve(res)
      })
      .catch(err => {
        reject(err)
      })
  })
}
const PUT = function (url: string, data: object) {
  return new Promise((resolve, reject) => {
    request({
      url,
      method: 'put',
      data
    })
      .then(res => {
        resolve(res)
      })
      .catch(err => {
        reject(err)
      })
  })
}

export { GET, postFrom, postJson, DELETE, PUT }
