import { GET, postFrom, postJson, DELETE, PUT } from './http'

export const getGroups = () => GET('/api/v1/groups')
