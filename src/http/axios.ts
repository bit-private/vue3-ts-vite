// @ts-nocheck
import axios from 'axios'
import { ElMessage, ElMessageBox } from 'element-plus'
import { removeToken, getToken } from '../assets/js/cookie'

const baseURL = import.meta.env.MODE == 'development' ? '' : import.meta.env.VITE_APP_BASE_API
// 创建axios实例
const request = axios.create({
  timeout: 60000,
  //   baseURL,
  withCredentials: true // 跨域访问需要发送cookie时一定要加
})

// axios的request拦截器
request.interceptors.request.use(
  config => {
    // Do something before request is sent
    // if (!config.data) {
    //   config.data = {
    //     timestamp: Date.parse(new Date())
    //   }
    // } else {
    //   config.data['timestamp'] = Date.parse(new Date())
    // }

    // 让每个请求携带token--['X-Token']为自定义key 请根据实际情况自行修改
    if (getToken()) {
      config.headers['Authorization'] = getToken()
    }
    // if (config.method === 'get') {
    //   config.url = config.url + '?' + stringify(config.data)
    // }

    // config.url = config.url.replace(/\|/g, '%7C') // 处理url的特殊字符|

    return config
  },
  error => {
    console.log(error)
    Promise.reject(error)
  }
)

// respone拦截器
request.interceptors.response.use(
  response => {
    const res = response.data
    //判断失败的code码并作出提示等操作
    if (res.code === 401) {
      Message.error(res.msg)
    }
    return Promise.resolve(response.data)
  },
  error => {
    if (error.response.status === 401) {
      Message.error('token失效，请重新登录！')
      setTimeout(() => {
        //   router.push('/login')
      }, 2000)
    } else {
      if (!window.navigator.onLine) {
        Message.warning('网络异常，请检查网络是否正常连接')
      } else if (error.code === 'ECONNABORTED') {
        Message.warning('请求超时')
      } else {
        Message.warning('服务器异常，请联系管理员')
      }
    }
    return Promise.reject(error) // 将错误继续返回给到具体页面
  }
)

export default request

function stringify(o) {
  let j = ''
  for (let i in o) {
    j = j + '&' + i + '=' + o[i]
  }
  return j.substr(1)
}
