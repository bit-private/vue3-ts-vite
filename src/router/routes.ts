import { RouteRecordRaw } from 'vue-router'
import Layout from '@/layout/container.vue'
import Login from '@/views/login/login.vue'

const layoutChildren: Array<RouteRecordRaw> = [
  //   {
  //     path: '/home',
  //     name: 'home',
  //     component: () => import('@/views/home/index.vue')
  //   }
]

const routes: Array<RouteRecordRaw> = [
  {
    path: '/',
    name: 'login',
    component: Login
  }
  //   {
  //     path: '/layout',
  //     name: 'layout',
  //     component: Layout,
  //     redirect: '/home',
  //     children: layoutChildren
  //   }
]

export default routes
