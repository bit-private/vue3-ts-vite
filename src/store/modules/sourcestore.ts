const sourcestore = {
  state: {
    // 图片格式
    imgType: ['JPG', 'PNG', 'GIF', 'SVG'],
    // 视频格式
    videoType: ['MP4', 'MKV', 'MOV', 'WEBM', 'OGG'],
    pathAllData: localStorage.getItem('File_Directory_Structure') ? JSON.parse(localStorage.getItem('File_Directory_Structure') as string) : [],
    pictures: sessionStorage.getItem('pictures') ? JSON.parse(sessionStorage.getItem('pictures') as string) : {}
  },
  mutations: {
    PATH_ALLDATA: (state: any, sourceParams: any) => {
      const { source, pictures } = sourceParams
      let arrSecondVar: object[] = []
      let referenceArr: string[] = []
      function s_to_ms(params: any) {
        let s: any = parseInt(params)
        let h: string = Math.floor(s / 60).toString()
        s = (s % 60).toString()
        h = h.length == 1 ? '0' + h : h
        s = s.length == 1 ? '0' + s : s
        return h + ':' + s
      }
      source.forEach((item: any) => {
        let urlArrVar: string[] = item.split('/')
        //  ['..', 'assets', 'sourceMaterial', '第一：技术创新', '多模融合', '1-支持加载多种格式模型，支持多种模型的融合.mp4']
        let sortNum: number = 0
        if (urlArrVar[urlArrVar.length - 1].indexOf('-') > 0) {
          sortNum = parseInt(urlArrVar[urlArrVar.length - 1].split('-')[0])
        }
        let typeVar = urlArrVar[6].split('.')[1].toUpperCase()
        let type: any = null
        if (state.imgType.indexOf(typeVar) != -1) {
          type = 'img'
        } else if (state.videoType.indexOf(typeVar) != -1) {
          type = 'video'
        } else {
          type = null
        }
        let name = urlArrVar[6].split('.')[0].split('(')[0]
        let seconds = urlArrVar[6].match(/\((\S*)\)/)
        let time = seconds ? s_to_ms(seconds[1]) : null
        arrSecondVar.push({
          url: item,
          province: urlArrVar[5],
          content: urlArrVar[6],
          sortNum,
          type,
          name,
          time
        })
        referenceArr.push(urlArrVar[5])
      })
      let params: object[] = []
      let setArr = Array.from(new Set(referenceArr))
      setArr.forEach(m => {
        let obj = { name: m, children: [] as any, type: '' }
        arrSecondVar.forEach((item: any) => {
          let { name, time, sortNum, url, type, content } = item
          let ibjCVar = JSON.stringify(obj.children)
          if (item.province == m) {
            if (type == 'video' && ibjCVar.indexOf(content) < 0) {
              obj.children.push({ time, sortNum, url, type, name, imgURL: '' })
            } else if (item.type == 'img' && ibjCVar.indexOf(item.name) > 0) {
              obj.children.forEach((f: any) => {
                if (f.name == name) {
                  f.imgURL = url
                }
              })
            } else if (type == 'img' && ibjCVar.indexOf(name) < 0) {
              obj.children.push({ time, sortNum, url, type, name, imgURL: url })
            }
          }
        })
        obj.type = obj.children[0].url.match(/\/sourceMaterial\/(\S*)\//)[1].split('/')[0]
        params.push(obj)
      })
      state.pathAllData = params
      state.pictures = pictures
      if (sessionStorage.getItem('pictures')) {
        sessionStorage.removeItem('pictures')
      }
      sessionStorage.setItem('pictures', JSON.stringify(pictures))
      if (localStorage.getItem('File_Directory_Structure')) {
        localStorage.removeItem('File_Directory_Structure')
      }
      localStorage.setItem('File_Directory_Structure', JSON.stringify(params))
    }
  },
  actions: {
    pathAction: (context: any) => {
      const files = import.meta.globEager('../../src/assets/sourceMaterial/**')
      const images: any = import.meta.globEager('../../src/assets/images/**')
      let pathAllData: string[] = []
      let imgCollection: any = {}
      for (const key in files) {
        pathAllData.push(files[key]['default'])
      }
      Object.keys(images).forEach((mq: any) => {
        let key: string = mq.match(/images\/(\S*)\./)[1]
        imgCollection[key] = images[mq]['default']
      })
      context.commit('PATH_ALLDATA', { source: pathAllData, pictures: imgCollection })
    }
  },
  getters: {}
}
export default sourcestore
