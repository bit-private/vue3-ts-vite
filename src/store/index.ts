import { createStore } from 'vuex'

import sourcestore from "./modules/sourcestore"

export default createStore({
  modules: {
    sourcestore
  }
})