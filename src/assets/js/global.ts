// @ts-nocheck

import CryptoJS from 'crypto-js'
/* 
    storage 名称前缀
    防止其他作为子应用时与主应用storage Key 冲突
*/
const storage_prefix = 'storagePrefix_'

/*  ***********加密工具类*********** */
const AES = {
  // 加密
  encode(data, defaultKey = 'abcdsxyzhkj12345') {
    let srcs = CryptoJS.enc.Utf8.parse(JSON.stringify(data))
    let key = CryptoJS.enc.Utf8.parse(defaultKey)
    let encrypted = CryptoJS.AES.encrypt(srcs, key, { mode: CryptoJS.mode.ECB, padding: CryptoJS.pad.Pkcs7 })
    return encrypted.toString()
  },
  //   解密
  decode(data, defaultKey = 'abcdsxyzhkj12345') {
    let key = CryptoJS.enc.Utf8.parse(defaultKey)
    let decrypt = CryptoJS.AES.decrypt(data, key, { mode: CryptoJS.mode.ECB, padding: CryptoJS.pad.Pkcs7 })
    let dataStr = CryptoJS.enc.Utf8.stringify(decrypt).toString()
    return JSON.parse(dataStr)
  }
}
// MD5加密
// export const MD5 = {
//   encode(data) {
//     return CryptoJS.MD5(data).toString() /* toString后会变成Hex 32位字符串*/
//   }
// }
// // base64加密
// export const Base64 = {
//   // 加密
//   encode(str) {
//     return btoa(
//       encodeURIComponent(str).replace(/%([0-9A-F]{2})/g, function toSolidBytes(match, p1) {
//         return String.fromCharCode('0x' + p1)
//       })
//     )
//   },
//   // 解密
//   decode(str) {
//     // Going backwards: from bytestream, to percent-encoding, to original string.
//     return decodeURIComponent(
//       atob(str)
//         .split('')
//         .map(function (c) {
//           return '%' + ('00' + c.charCodeAt(0).toString(16)).slice(-2)
//         })
//         .join('')
//     )
//   }
// }

/*  ***********storage存储加密*********** */
const storage = {
  // 设置存储
  set: function (key, value, type = 'session') {
    if (key == '' || !key) {
      console.log('缺少key')
      return
    }
    let aesKey = AES.encode(storage_prefix + key)
    let aesValue = AES.encode(value)
    window[type + 'Storage'].setItem(aesKey, aesValue)
  },
  //   获取存储
  get: function (key, type = 'session') {
    if (key == '' || !key) {
      console.log('缺少key')
      return
    }
    let aesKey = AES.encode(storage_prefix + key)
    let str = window[type + 'Storage'].getItem(aesKey)
    if (str) {
      return AES.decode(str)
    } else {
      return null
    }
  },
  //   删除存储
  clear: function (key, type = 'session') {
    if (key == '' || !key) {
      console.log('缺少必要参数')
      return
    }
    let aesKey = AES.encode(storage_prefix + key)
    window[type + 'Storage'].removeItem(aesKey)
  },
  //   清空存储
  clearAll: function () {
    let sessKeys = Object.keys(sessionStorage)
    let localKeys = Object.keys(localStorage)
    for (const key in sessKeys) {
      if (AES.decode(key).includes(storage_prefix)) {
        sessionStorage.removeItem(key)
      }
    }
    for (const key in localKeys) {
      if (AES.decode(key).includes(storage_prefix)) {
        localStorage.removeItem(key)
      }
    }
  }
}
// echarts图表字体自适应
const selfAdaption = function (res) {
  let clientWidth = window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth
  if (!clientWidth) return
  //   设计稿尺寸1920
  let fontSize = clientWidth / 1920
  return res * fontSize
}

export default {
  //   Base64,
  AES,
  //   MD5,
  storage,
  selfAdaption
}
