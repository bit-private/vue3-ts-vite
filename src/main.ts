import { createApp } from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import ElementPlus from 'element-plus'
import 'element-plus/dist/index.css'
import zhCn from 'element-plus/lib/locale/lang/zh-cn'
import './assets/js/flexible'
import PublicGF from './assets/js/global'

const app = createApp(App)
/** 全局函数 */
app.config.globalProperties.$PublicGF = PublicGF
/** 注册全局组件
 *  使用方法 <G-map></G-map>
 *  由于vue3 setup 语法没有组件名属性name，只能用文件名代替
 *  G-自定义前缀  map为文件名
 */
const components: any = import.meta.globEager('./components/global/*.vue')
for (const key in components) {
  let k = key.match(/global\/(\S*)\.vue/)
  if (k && k.length > 1) {
    let name = 'G-' + k[1]
    app.component(name, components[key].default)
  }
}

app.use(ElementPlus, { locale: zhCn }).use(router).use(store).mount('#app')
