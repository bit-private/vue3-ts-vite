import { defineConfig, loadEnv } from 'vite'
import vue from '@vitejs/plugin-vue'
// import path from 'path'
// const { resolve } = require('path')

// function _resolve(dir: string) {
//   return path.resolve(__dirname, dir)
// }
export default ({ mode }: { mode: string }): unknown => {
  const target = loadEnv(mode, process.cwd()).VITE_APP_BASE_API
  return defineConfig({
    plugins: [vue()],
    // base: './',
    resolve: {
      extensions: ['.js', '.ts', '.jsx', '.tsx', '.json'],
      alias: {
        '@': '/src'
      }
    },
    build: {
      outDir: 'dist',
      assetsDir: 'static'
    },

    server: {
      https: false,
      open: true,
      port: 9010,
      host: '0.0.0.0',
      proxy: {
        //   '/api': {
        //     target: 'http://192.168.1.195:8990', // 后台接口
        //     changeOrigin: true,
        //     secure: false, // 如果是https接口，需要配置这个参数
        //     // ws: true, //websocket支持
        //     rewrite: path => path.replace(/^\/api/, '')
        //   }
        '/api': target
      }
    },

    // 引入第三方的配置

    optimizeDeps: {
      include: []
    }
  })
}
